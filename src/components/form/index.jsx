import React, { useState } from "react";
import { Form, Input, Button } from "antd";
const AntdForm = () => {
    const defaultValues = {
        name: { value: "", error: "" },
        email: { value: "", error: "" },
        password: { value: "", error: "" },
        confirmPassword: { value: "", error: "" },
      };
  const [errorMessage, setErrorMessage] = useState();
  const [formValues, setFormValues] = useState(defaultValues);
  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };
  const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
  };
 

  const handleSubmit = (e) => {
    if (formValues.name.value.length <= 0) {
        formValues.name.error="digite seu nome"
        setErrorMessage   (<div style={{ color: "red" }}>{formValues.name.error}</div>);
    }
    else if(formValues.password.value<=3){
        formValues.password.error="digite uma senha maior que 3 characteres"
        setErrorMessage   (<div style={{ color: "red" }}>{formValues.password.error}</div>);
    }
    else if(formValues.confirmPassword.value<=0){
        formValues.confirmPassword.error="digite novamente sua senha"
        setErrorMessage   (<div style={{ color: "red" }}>{formValues.confirmPassword.error}</div>);
    }
    else if(formValues.password.value!==formValues.confirmPassword.value){
        formValues.password.error="as senhas divergem"
        setErrorMessage   (<div style={{ color: "red" }}>{formValues.password.error}</div>);
    }
    else{
        setErrorMessage(<div style={{ color: "blue" }}>Enviado com sucesso</div>)
    }
 
  };
  const setName = ({ target: { value } }) => {
    setFormValues({ ...formValues, name: { value } });
  };
  const setPassword = ({ target: { value } }) => {
    setFormValues({ ...formValues, password: { value } });
  };
  const setConfirmPassword = ({ target: { value } }) => {
    setFormValues({ ...formValues, confirmPassword: { value } });
  };
  return (
    <Form {...layout} onFinish={handleSubmit}>
      <Form.Item label="Nome" name="name" onChange={setName} value={formValues.name}>
        <Input />
      </Form.Item>
      <Form.Item
        {...tailLayout}
        name={"email"}
        label="Email"
        rules={[{ type: "email" }]}
      >
        <Input />
      </Form.Item>
      <Form.Item {...tailLayout} label="Senha" name="password">
        <Input type="password" onChange={setPassword} />
      </Form.Item>
      <Form.Item
        {...tailLayout}
        label="Confirme sua senha"
        name="confirmPassword"
        onChange={setConfirmPassword}
      >
        <Input type="password" />
      </Form.Item>
      <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
      {errorMessage}
    </Form>
  );
};
export default AntdForm;
